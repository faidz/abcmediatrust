@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">{{ __('Posts') }}</div>
                    <br>
                    <form class="form-inline" method="GET" action="{{route('list-post')}}">
                        <div class="form-group mx-sm-3 mb-2">
                            <label for="post" class="sr-only">Post</label>
                            <input type="text" class="form-control" name="filter[filter]" id="post"
                                   placeholder="Search Post">
                        </div>
                        <button type="submit" class="btn btn-primary mb-2">Search</button>
                    </form>
                    <div class="float-right py-3 px-3">
                        <a href="{{route('post-create')}}" class="btn btn-success">Create</a>
                    </div>@include('errors')
                    <div class="card-body">
                        <table class="table">
                            <thead>
                            <tr>
                                <th>No.</th>
                                <th>Title</th>
                                <th>Body</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($posts as $i => $post)
                                <tr>
                                    <td>{{($i+=1)}}</td>
                                    <td>{{$post->title}}</td>
                                    <td>{{$post->body}}</td>
                                    <td>
                                        <div class="col-md-8 offset-md-4">
                                            <a href="{{route('update-post', $post->id)}}"
                                               class="btn btn-primary">Edit</a>
                                            <form method="POSt" action="{{route('delete-post', $post->id)}}">
                                                @csrf
                                                @method('DELETE')
                                                <button type="submit" class="btn btn-danger">
                                                    {{ __('Delete') }}
                                                </button>
                                            </form>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            @if(empty($post))
                                <tr>
                                    <td colspan="4" class="text-center alert alert-warning">No result found.</td>
                                </tr>
                            @endif
                            </tbody>
                            <tfoot>
                            <tr>
                                <td colspan="4">{{$posts->links()}}</td>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
