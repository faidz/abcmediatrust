<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::prefix('posts')->group(function () {
    Route::get('/create', function () {
        return view('posts.create');
    })->name('post-create');
    Route::post('/', [\App\Http\Controllers\Posts\CreatePost::class, '__invoke'])->name('create-post');
    Route::get('/', [\App\Http\Controllers\Posts\ListPost::class, '__invoke'])->name('list-post');
    Route::get('/{id}', [\App\Http\Controllers\Posts\ShowPost::class, '__invoke'])->name('show-post');
    Route::put('/{id}', [\App\Http\Controllers\Posts\UpdatePost::class, '__invoke'])->name('update-post');
    Route::delete('/{id}', [\App\Http\Controllers\Posts\DeletePost::class, '__invoke'])->name('delete-post');
});
