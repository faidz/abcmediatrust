
## About this project

Basic CRUD operation
## Local Environment Setup

```bash
# Clone
$ git clone https://faidz@bitbucket.org/faidz/abcmediatrust.git

# Install dependencies
$ composer install

# Database setup
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=abcmediatrust
DB_USERNAME=root
DB_PASSWORD=

# Migration and seeding
$ php artisan migrate --seed

# Unit testing
$ vendor/bin/phpunit

# serve at localhost:8000
$ php artisan serve
```
