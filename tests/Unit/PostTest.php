<?php

namespace Tests\Unit;

use App\Models\Post;
use App\Repositories\PostRepository;
use App\Services\PostService;
use Illuminate\Container\Container;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use Tests\BaseTestCase;

/**
 * Class PostTest
 * @package Tests\Unit
 */
class PostTest extends BaseTestCase
{
    /** @test */
    public function it_can_list_a_posts()
    {
        $service = $this->getService();

        $data = $service->list(['per_page' => 5]);

        $this->assertEquals(5, $data->count());
        $this->assertArrayHasKey('data', $data->toArray());
    }

    /** @test */
    public function it_has_an_error_in_adding_post()
    {
        $this->expectException(QueryException::class);

        $service = $this->getService();

        $service->create([]);
    }

    /** @test */
    public function it_can_add_a_post()
    {
        $service = $this->getService();

        $post = $service->create([
            'title' => 'Post Title',
            'body' => 'Body'
        ]);

        $this->assertInstanceOf(Post::class, $post);
        $this->assertEquals('Post Title', $post->title);
    }

    /** @test */
    public function it_can_update_a_post()
    {
        $post = $this->createPostData();

        $service = $this->getService();

        $updated = $service->update($post->id, [
            'title' => 'Updated post'
        ]);

        $updatedPost = $service->find($post->id);

        $this->assertTrue($updated);
        $this->assertEquals('Updated post', $updatedPost->title);
    }

    /** @test */
    public function it_can_find_a_post()
    {
        $post = $this->createPostData();

        $service = $this->getService();

        $target = $service->find($post->id);

        $this->assertInstanceOf(Post::class, $target);
        $this->assertEquals($post->title, $target->title);
    }

    /** @test */
    public function it_has_an_error_finding_a_post()
    {
        $this->expectException(ModelNotFoundException::class);

        $service = $this->getService();

        $service->find(0);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|mixed
     */
    private function createPostData()
    {
        return factory(Post::class)->create();
    }
    /**
     * @return PostService
     * @throws \Exception
     */
    private function getService()
    {
        return new PostService(new PostRepository(Container::getInstance()));
    }
}
