<?php

namespace Tests;

use Faker\Generator;

/**
 * Class BaseTestCase
 * @package Tests
 */
class BaseTestCase extends TestCase
{
    private $faker;

    /**
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
    }
}
