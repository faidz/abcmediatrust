<?php

namespace App\Http\Controllers\Posts;

use App\Http\Controllers\Controller;
use App\Services\PostService;

/**
 * Class ShowPost
 * @package App\Http\Controllers\Posts
 */
class ShowPost extends Controller
{
    /**
     * @param PostService $sectionService
     * @param string $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function __invoke(PostService $sectionService, string $id)
    {
        $post = $sectionService->find($id);

        return view('posts.edit', compact('post'));
    }
}
