<?php

namespace App\Http\Controllers\Posts;

use App\Http\Controllers\Controller;
use App\Services\PostService;

/**
 * Class DeletePost
 * @package App\Http\Controllers\Posts
 */
class DeletePost extends Controller
{
    /**
     * @param PostService $sectionService
     * @param string $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function __invoke(PostService $sectionService, string $id)
    {
        $sectionService->delete($id);

        $posts = $sectionService->list([]);

        return view('posts.index', compact('posts'));
    }
}
