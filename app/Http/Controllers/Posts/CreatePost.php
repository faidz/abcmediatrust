<?php

namespace App\Http\Controllers\Posts;

use App\Http\Controllers\Controller;
use App\Http\Requests\CreatePostRequest;
use App\Services\PostService;

/**
 * Class CreatePost
 * @package App\Http\Controllers\Posts
 */
class CreatePost extends Controller
{
    /**
     * @param PostService $sectionService
     * @param CreatePostRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function __invoke(PostService $sectionService, CreatePostRequest $request)
    {
        $sectionService->create($request->payload());

        $request->session()->flash('message', 'Create successful');

        return redirect()->route('list-post');
    }
}
