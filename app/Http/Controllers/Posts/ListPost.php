<?php

namespace App\Http\Controllers\Posts;

use App\Http\Controllers\Controller;
use App\Services\PostService;
use Illuminate\Http\Request;

/**
 * Class ListPost
 * @package App\Http\Controllers\Backend\Sections
 */
class ListPost extends Controller
{
    /**
     * @param PostService $sectionService
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function __invoke(PostService $sectionService, Request $request)
    {
        $posts = $sectionService->list($request->only('filter', 'per_page'));

        return view('posts.index', compact('posts'));
    }
}
