<?php

namespace App\Http\Controllers\Posts;

use App\Http\Controllers\Controller;
use App\Http\Requests\UpdatePostRequest;
use App\Services\PostService;

/**
 * Class UpdatePost
 * @package App\Http\Controllers\Posts
 */
class UpdatePost extends Controller
{
    /**
     * @param PostService $sectionService
     * @param UpdatePostRequest $request
     * @param string $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function __invoke(PostService $sectionService, UpdatePostRequest $request, string $id)
    {
        $sectionService->update($id, $request->payload());

        $request->session()->flash('message', 'Update successful');

        return redirect()->route('list-post');
    }
}
