<?php

namespace App\Repositories;

use App\Models\Post;
use App\Support\Pagination;
use Illuminate\Support\Arr;

/**
 * Class PostRepository
 * @package App\Repositories
 */
class PostRepository extends BaseRepository
{
    /**
     * @return string
     */
    public function model()
    {
        return Post::class;
    }

    /**
     * @param array $data
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function list(array $data)
    {
        $perPage = Arr::get($data, 'per_page', Pagination::DEFAULT_LIMIT);
        $filter = Arr::get($data, 'filter', []);

        return $this->wherePost($filter)
            ->orderBy()
            ->paginate($perPage);
    }

    /**
     * @param $filter
     * @return $this|PostRepository
     */
    private function wherePost($filter)
    {
        if (!isset($filter['filter'])) {
            return $this;
        }

        $this->where('title', 'LIKE', '%' . $filter['filter'] . '%');
        $this->orWhere('body', 'LIKE', '%' . $filter['filter'] . '%');

        return $this;
    }
}
