<?php

namespace App\Services;

use App\Repositories\PostRepository;
use Illuminate\Support\Facades\Cache;

/**
 * Class PostService
 * @package App\Services
 */
class PostService
{
    private const KEY_CACHE = 'posts';

    /**
     * @var PostRepository
     */
    private $postRepository;

    /**
     * GradeService constructor.
     * @param PostRepository $postRepository
     */
    public function __construct(PostRepository $postRepository)
    {
        $this->postRepository = $postRepository;
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function list(array $data = [])
    {
        return Cache::remember(self::KEY_CACHE, 3600, function () use ($data) {
            return $this->postRepository->list($data);
        });
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function create(array $data)
    {
        Cache::forget(self::KEY_CACHE);

        return $this->postRepository->create($data);
    }

    /**
     * @param string $id
     * @param array $data
     * @return bool
     */
    public function update(string $id, array $data)
    {
        Cache::forget(self::KEY_CACHE);

        return $this->postRepository->update($id, $data);
    }

    /**
     * @param string $id
     * @return mixed
     */
    public function find(string $id)
    {
        return $this->postRepository->find($id);
    }

    /**
     * @param string $id
     * @return int
     */
    public function delete(string $id)
    {
        Cache::forget(self::KEY_CACHE);

        return $this->postRepository->delete($id);
    }
}
